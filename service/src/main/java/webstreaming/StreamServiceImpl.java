package webstreaming;

import java.util.concurrent.SubmissionPublisher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import webstreaming.proto.StreamData;
import webstreaming.proto.StreamJoin;
import webstreaming.proto.StreamingServiceGrpc;

@GrpcService
public class StreamServiceImpl extends StreamingServiceGrpc.StreamingServiceImplBase {
    private static final Logger log = LogManager.getLogger(StreamServiceImpl.class);

    @Autowired
    private SubmissionPublisher<StreamData> publisher;

    @Override
    public void join(StreamJoin request, StreamObserver<StreamData> responseObserver) {
        log.info("join | requiest: {}", request.getMessage());

        publisher.subscribe(new StreamSubscriber(request, responseObserver));
    }
}
