package webstreaming;

import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.grpc.stub.StreamObserver;
import webstreaming.proto.StreamData;
import webstreaming.proto.StreamJoin;

public class StreamSubscriber implements Subscriber<StreamData> {
    private static final Logger log = LogManager.getLogger(StreamSubscriber.class);

    private AtomicInteger count = new AtomicInteger(0);
    private Subscription subscription;

    private StreamJoin request;
    private StreamObserver<StreamData> observer;

    public StreamSubscriber(StreamJoin request, StreamObserver<StreamData> observer) {
        this.count = new AtomicInteger(0);
        this.request = request;
        this.observer = observer;
    }

    @Override
    public void onComplete() {
        log.debug("subscriber has been completed.");
    }

    @Override
    public void onError(Throwable t) {
        log.debug("a problem occured in subscriber: ", t);
    }

    @Override
    public void onNext(StreamData reply) {
        Integer currentCount = count.incrementAndGet();
        StreamData newMessage = StreamData.newBuilder(reply).setMessage(
                String.format("%s Seqno: %s Request: %s", reply.getMessage(), currentCount, request.getMessage()))
                .build();
        log.debug("send | new: {}", newMessage.getMessage());
        observer.onNext(newMessage);
        this.subscription.request(1);
    }

    @Override
    public void onSubscribe(Subscription subscription) {
        log.debug("subscribing subscriber");
        this.subscription = subscription;
        this.subscription.request(1);
    }
}
