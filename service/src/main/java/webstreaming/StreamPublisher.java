package webstreaming;

import java.util.concurrent.SubmissionPublisher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import webstreaming.proto.StreamData;

@Component
public class StreamPublisher implements Runnable {
    private static final Logger log = LogManager.getLogger(StreamPublisher.class);

    @Autowired
    private SubmissionPublisher<StreamData> publisher;

    @Override
    public void run() {
        int i = 0;
        while (!Thread.currentThread().isInterrupted()) {
            i++;
            StreamData reply = StreamData.newBuilder().setMessage(
                    String.format("GlobalSeqno: %s", i)).build();
            Integer n = publisher.getNumberOfSubscribers();
            log.info("publishing new tick | id: {} to: {}", i, n);
            publisher.submit(reply);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                return;
            }
        }
    }
}