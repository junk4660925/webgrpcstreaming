package webstreaming;

import java.util.concurrent.SubmissionPublisher;

import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import webstreaming.proto.StreamData;

@Configuration
public class StreamConfig {
    @Bean
    public SubmissionPublisher<StreamData> createPublisher() {
        return new SubmissionPublisher<>();
    }

    @Bean
    public ApplicationRunner streamPublisherRunner(StreamPublisher publisher) {
        return args -> new Thread(publisher).start();
    }
}
