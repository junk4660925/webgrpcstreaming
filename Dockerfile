FROM eclipse-temurin:17-jdk-jammy

WORKDIR /app

COPY .mvn/ .mvn
COPY mvnw pom.xml ./
COPY service/pom.xml service/pom.xml 
COPY proto/pom.xml proto/pom.xml

RUN ./mvnw dependency:go-offline -pl proto
COPY proto ./proto
RUN ./mvnw install -pl proto

RUN ./mvnw dependency:go-offline -pl service
COPY service ./service
RUN ./mvnw install -pl service

COPY run.sh ./

CMD ["./run.sh"]
