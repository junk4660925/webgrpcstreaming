#!/usr/bin/env bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd ${SCRIPT_DIR}

# Directory to write generated code to (.js and .d.ts files)
OUT_DIR="./src/proto"
rm -rf ${OUT_DIR}
mkdir -p ${OUT_DIR}

# protoc \
#     -I../proto/src/main/ \
#     --plugin="protoc-gen-ts=./node_modules/.bin/protoc-gen-ts" \
#     --plugin="protoc-gen-grpc-web=./node_modules/.bin/protoc-gen-grpc-web" \
#     --js_out="import_style=commonjs:${OUT_DIR}" \
#     --ts_out="${OUT_DIR}" \
#     webstreaming/proto/stream.proto

protoc \
    -I../proto/src/main/proto/ \
    --plugin="protoc-gen-ts=./node_modules/.bin/protoc-gen-ts" \
    --plugin="protoc-gen-grpc-web=./node_modules/.bin/protoc-gen-grpc-web" \
    --js_out="import_style=commonjs:${OUT_DIR}" \
    --grpc-web_out="import_style=commonjs+dts,mode=grpcwebtext:${OUT_DIR}" \
    stream.proto
