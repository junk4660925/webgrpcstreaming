import React, { useReducer } from 'react';
import logo from './logo.svg';
import './App.css';
import * as grpcWeb from 'grpc-web';
import { StreamingServiceClient } from './proto/stream_grpc_web_pb'
import { StreamJoin, StreamData } from './proto/stream_pb';

const Service = new StreamingServiceClient(`/`)

function request() {
  let req = new StreamJoin()
  req.setMessage("Hello from React!")
  Service.join(req).on(
    'data', (res) => {
      console.log(`receive message ${res.getMessage()}`)
    }
  ).on(
    'end', () => {
      console.log(`finished`)
    }
  ).on(
    'status', (status) => {
      console.log(`status code ${status.code}`)
    }
  )
}

request()

interface AppState {
  stream?: grpcWeb.ClientReadableStream<StreamData>
  data: string
}

class App extends React.Component {
  state: AppState = { data: '' };
  public delete = () => {
    this.setState({ display: false });
  };

  private stopStream() {
    if (this.state.stream) {
      this.state.stream.cancel()
      this.setState({ stream: null })
    }
  }

  private onMessage(message: StreamData) {
    this.setState({ data: message.getMessage() })
  }

  private startStream() {
    if (!this.state.stream) {
      let req = new StreamJoin()
      req.setMessage("Hello from React!")
      let stream = Service.join(req).on(
        'data', (res) => {
          console.log(`receive message ${res.getMessage()}`)
          this.onMessage(res)
        }
      ).on(
        'end', () => {
          console.log(`finished`)
        }
      ).on(
        'status', (status) => {
          console.log(`status code ${status.code}`)
        }
      )
      this.setState({ stream })
    }
  }

  public componentDidMount() {
    this.startStream()
  }

  public componentWillUnmount() {
    this.stopStream()
  }
 
  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            <code>
              {this.state.data}
            </code>
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}
 

export default App;
