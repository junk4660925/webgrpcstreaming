import * as grpcWeb from 'grpc-web';

import * as stream_pb from './stream_pb';


export class StreamingServiceClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  join(
    request: stream_pb.StreamJoin,
    metadata?: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<stream_pb.StreamData>;

}

export class StreamingServicePromiseClient {
  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; });

  join(
    request: stream_pb.StreamJoin,
    metadata?: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<stream_pb.StreamData>;

}

