import * as jspb from 'google-protobuf'



export class StreamJoin extends jspb.Message {
  getMessage(): string;
  setMessage(value: string): StreamJoin;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StreamJoin.AsObject;
  static toObject(includeInstance: boolean, msg: StreamJoin): StreamJoin.AsObject;
  static serializeBinaryToWriter(message: StreamJoin, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StreamJoin;
  static deserializeBinaryFromReader(message: StreamJoin, reader: jspb.BinaryReader): StreamJoin;
}

export namespace StreamJoin {
  export type AsObject = {
    message: string,
  }
}

export class StreamData extends jspb.Message {
  getMessage(): string;
  setMessage(value: string): StreamData;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StreamData.AsObject;
  static toObject(includeInstance: boolean, msg: StreamData): StreamData.AsObject;
  static serializeBinaryToWriter(message: StreamData, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StreamData;
  static deserializeBinaryFromReader(message: StreamData, reader: jspb.BinaryReader): StreamData;
}

export namespace StreamData {
  export type AsObject = {
    message: string,
  }
}

